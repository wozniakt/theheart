﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class PlayerModel
{
	public int MaxHealth;
	public int CurrentHealth;
	public int CurrentPoints;
	public int HighscorePoints;
}

