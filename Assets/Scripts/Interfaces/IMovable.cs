﻿using UnityEngine;
using System.Collections;
using System;
public interface IMovable 
{
	void StartMove(Transform target=null);
	void StartMove();
	void StopMove();
	
}

