﻿using UnityEngine;
using System.Collections;

public interface IGiveDamage 
{
	int DamageAmount{get;set;}
	void OnTriggerEnter (Collider other);
}

