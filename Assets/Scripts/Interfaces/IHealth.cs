﻿using UnityEngine;
using System.Collections;

public interface IHealth 
{
	int MaxHealth{ get; set; }
	int Health{ get; set; }
	void ChangeHealth(int healthChangeValue);
}

