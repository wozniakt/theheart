﻿using UnityEngine;
using System.Collections;

public class PanelMenuController : MonoBehaviour
{
	[SerializeField]
	GameObject MenuPanel;
	void OnEnable ()
	{
		EventsManager.StartListening ("GameIsOn", DisableStartPanel);
	}
	void EnableStartPanel ()
	{
		MenuPanel.SetActive (true);
	}

	void DisableStartPanel ()
	{
		MenuPanel.SetActive (false);
	}
}

