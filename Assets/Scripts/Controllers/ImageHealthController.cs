﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class ImageHealthController : MonoBehaviour
{
	public GameObject HeartGo;
	IHealth heartHealth;
	[SerializeField]
	Image healthImage;

	void Awake ()
	{
		EventsManager.StartListening ("HeartGotHit", UpdateImageHealth);
		heartHealth=HeartGo.GetComponent<IHealth> ();
		UpdateImageHealth ();
	}

	void UpdateImageHealth()
	{
		healthImage.fillAmount = (float)heartHealth.Health / heartHealth.MaxHealth;
	}
}

