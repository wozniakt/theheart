﻿using UnityEngine;
using System.Collections;

public class PanelHudController : MonoBehaviour
{
	[SerializeField]
	GameObject HudPanel;

	void Start ()
	{
		EventsManager.StartListening ("GameIsOn",EnableHudPanel);
	}

	void EnableHudPanel ()
	{
		HudPanel.SetActive (true);
	}

	void DisableHudPanel ()
	{
		FactorySingletons.Instance.PlayerData.CurrentPlayer.CurrentPoints = 0;
		HudPanel.SetActive (false);
	}
}

