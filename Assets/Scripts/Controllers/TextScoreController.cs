﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextScoreController : MonoBehaviour
{
	public Text CurrentPointsText;
	public Text HighScoreText;

	void Start ()
	{
		EventsManager.StartListening ("PointsChange",ChangePointsText);
		ChangePointsText ();
	}

	void ChangePointsText ()
	{
		CurrentPointsText.text ="SCORE: "+ FactorySingletons.Instance.PlayerData.CurrentPlayer. CurrentPoints.ToString();
		HighScoreText.text ="HIGHSCORE: "+ FactorySingletons.Instance.PlayerData.CurrentPlayer.HighscorePoints.ToString();
	}

}

