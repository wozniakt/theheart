﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(PoolingComponent))]
public class SpawnerEnemiesComponent : MonoBehaviour
{
	[SerializeField]
	PoolingComponent spawnComponent;
	public Transform TargetToDestroy;
	[SerializeField]
	float minDelay=5f, maxDelay=10f;
	void Awake()
	{
		spawnComponent = GetComponent<PoolingComponent> ();
	}

	void OnEnable()
	{
		EventsManager.StartListening ("ImageTargetFound", SpawnEnemies);

	}
	void OnDisable()
	{
		EventsManager.StopListening ("ImageTargetFound", SpawnEnemies);
	}
	void SpawnEnemies()
	{
		StartCoroutine (SpawningEnemies ());
	}
	IEnumerator SpawningEnemies()
	{
		float rnd = Random.Range (minDelay, maxDelay);

		yield return new WaitForSeconds (rnd);
		GameObject enemy = spawnComponent.Spawn ();
		enemy.transform.SetParent (this.transform);
		enemy.SetActive (true);
		enemy.transform.position = this.transform.position;
		enemy.GetComponent<IMovable> ().StartMove (TargetToDestroy);
		yield return new WaitForSeconds (rnd);
		StartCoroutine (SpawningEnemies ());
	}
}

