﻿using UnityEngine;
using System.Collections;

public class EnemyDamageComponent : MonoBehaviour, IGiveDamage
{

	[SerializeField]int damageAmount;
	#region IGiveDamage implementation
	public int DamageAmount {
		get {
			return damageAmount;
		}
		set {
			damageAmount=value;
		}
	}

	public void OnTriggerEnter(Collider other)
	{
		IHealth healthComponent = other.GetComponent<IHealth> ();

		if (other.GetComponent<IHealth>()!=null && other.CompareTag("Player")) {
			healthComponent.ChangeHealth (DamageAmount);
			EventsManager.TriggerEvent ("HeartGotHit");
			this.gameObject.SetActive (false);
		}
	}
	#endregion
}

