﻿using UnityEngine;
using System.Collections;

public class DeathEnemyComponent : MonoBehaviour, IDeath
{
	#region IDeath implementation
	public void Die ()
	{
		this.gameObject.SetActive (false);
		EventsManager.TriggerEvent ("PointsChange");
	}
	#endregion


}

