﻿using UnityEngine;
using System.Collections;

public class BulletDamageComponent : MonoBehaviour, IGiveDamage
{
	[SerializeField]int damageAmount;
	#region IGiveDamage implementation
	public int DamageAmount {
		get {
			return damageAmount;
		}
		set {
			damageAmount=value;
		}
	}

	public void OnTriggerEnter(Collider other)
	{
		IHealth healthComponent = other.GetComponent<IHealth> ();
		if (other.GetComponent<IHealth>()!=null &&  other.CompareTag("Player") ==false){
		{
			GameObject effect =	FactorySingletons.Instance.PoolManager.SpawnEffect ();
			effect.transform.position = transform.position;
			effect.SetActive (true);
			healthComponent.ChangeHealth (DamageAmount);
		}
		this.gameObject.SetActive (false);
	}
	#endregion
	}

}
