﻿using UnityEngine;
using System.Collections;

public class HeartAnimComponent : MonoBehaviour
{
	[SerializeField]
	Animator animator;

	public void ChangeAnimSpeed(float speed)
	{
		animator.speed = speed;
	}
	

}

