﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class PlayerShootingComponent : MonoBehaviour
{
	public Camera ARCamera;
	public float shotPower;
	public GameObject bulletPrefab;
	public GameObject cursor;
	[SerializeField]
	PoolingComponent PoolComponent;
	void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			if (!IsPointerOverUIObject()) {
				ShootBullet ();
			}
		}
	}

	private bool IsPointerOverUIObject() {
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
		return results.Count > 0;
	} 


	public void ShootBullet(){

		GameObject bullet = PoolComponent.Spawn ();
		bullet.SetActive(true);
		bullet.transform.position =ARCamera.transform.position;
		bullet.transform.rotation = ARCamera.transform.rotation;

	}
}