﻿using UnityEngine;
using System.Collections;

public class HeartSoundComponent : MonoBehaviour
{
	[SerializeField] AudioSource audioSrc;

	public void ChangeHeartSoundRate (float speed)
	{

		audioSrc.pitch = speed;
	}


}

