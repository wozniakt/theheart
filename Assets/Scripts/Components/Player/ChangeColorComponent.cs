﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChangeColorComponent : MonoBehaviour
{
	[SerializeField]
	MeshRenderer meshRenderer;
	[SerializeField]
	List<Color> getHitColors;
	int currentColorIndex;
	// Use this for initialization
	void OnEnable()
	{
		currentColorIndex = 0;
		meshRenderer.materials [0].color = getHitColors [currentColorIndex];
	}
	public void ChangeColor ()
	{
		currentColorIndex++;
		meshRenderer.materials [0].color = getHitColors [Mathf.Min(getHitColors.Count-1,currentColorIndex )];
	}
	

}

