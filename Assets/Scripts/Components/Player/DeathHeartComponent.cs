﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DeathHeartComponent : MonoBehaviour, IDeath
{
	[SerializeField]
	float delayAfterDeath=1f;
	[SerializeField]
	Animator animator;
	#region IDeath implementation
	public void Die ()
	{
		StartCoroutine (Dying ());

	}
	#endregion
	IEnumerator Dying()
	{
		animator.enabled = false;
		this.transform.localScale = Vector3.zero;
		for (int i = 0; i < 1; i++) {
			GameObject effect= GetComponent<PoolingComponent> ().Spawn ();
			effect.SetActive (true);
			effect.transform.position = this.transform.position;
			yield return new WaitForSeconds (0.1f);
		}

		yield return new WaitForSeconds (delayAfterDeath);
		FactorySingletons.Instance.PlayerData.CurrentPlayer.CurrentPoints = 0;
		SceneManager.LoadScene (0);

	}
}