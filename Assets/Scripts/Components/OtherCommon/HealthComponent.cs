﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ChangeColorComponent))]
public class HealthComponent : MonoBehaviour,IHealth
{


	[SerializeField]
	ChangeColorComponent changeColorComponent;
	HeartAnimComponent heartAnimComponent;
	HeartSoundComponent heartSoundComponent;
	#region IHealth implementation
	[SerializeField] int maxHealth;
	[SerializeField] int health;
	public int MaxHealth {
		get {
			return maxHealth;
		}
		set {
			maxHealth=value;
		}
	}

	public int Health {
		get {
			return health;
		}
		set {
			health=value;
			changeColorComponent.ChangeColor ();
			if (heartAnimComponent!=null && heartSoundComponent!=null) {
				heartAnimComponent.ChangeAnimSpeed ( MaxHealth- health+1);
				heartSoundComponent.ChangeHeartSoundRate( MaxHealth- health+1);
			}
			if (health<=0) {
				GetComponent<IDeath> ().Die ();
			}
		}
	}
	void Awake()
	{
		heartAnimComponent = GetComponent<HeartAnimComponent> (); 
		heartSoundComponent = GetComponent<HeartSoundComponent> ();
		Health = MaxHealth;
	}
	public void ChangeHealth (int healthChangeValue)
	{
		Health=Health - healthChangeValue;
	}
	#endregion


}

