﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolingComponent : MonoBehaviour {


	public GameObject objectToSpawn;
	public int pooledAmount = 20;
	public bool shouldExpand = true;

	public List<GameObject> SpawnedObjects;

	void Start ()
	{
		SpawnedObjects = new List<GameObject>();
		for(int i = 0; i < pooledAmount; i++)
		{
			GameObject obj = (GameObject)Instantiate(objectToSpawn);
			obj.SetActive(false);
			SpawnedObjects.Add(obj);
			obj.transform.SetParent (this.transform);
		}
	}

	public GameObject Spawn()
	{

		for(int i = 0; i< SpawnedObjects.Count; i++)
		{
			if(SpawnedObjects[i] == null)
			{
				GameObject spawnedObject = (GameObject)Instantiate(objectToSpawn);
				spawnedObject.SetActive(true);
				SpawnedObjects[i] = spawnedObject;
				spawnedObject.transform.SetParent (this.transform);
				return SpawnedObjects[i];
			}
			if(!SpawnedObjects[i].activeInHierarchy)
			{
				return SpawnedObjects[i];
			}    
		}

		if (shouldExpand)
		{
			GameObject obj = (GameObject)Instantiate(objectToSpawn);
			SpawnedObjects.Add(obj);
			return obj;
		}

		return null;
	}

}

