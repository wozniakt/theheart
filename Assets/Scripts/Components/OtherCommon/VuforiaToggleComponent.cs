﻿using UnityEngine;
using System.Collections;

public class VuforiaToggleComponent : MonoBehaviour
{
	[SerializeField]
	GameObject imageTargetGO;

	void Awake ()
	{
		EventsManager.StartListening ("ImageTargetFound", EnableImageTargetGO);
		EventsManager.StartListening ("ImageTargetLost", DisableImageTargetGO);
	}
	void OnDisable()
	{
		EventsManager.StopListening ("ImageTargetFound", EnableImageTargetGO);
		EventsManager.StopListening ("ImageTargetLost", DisableImageTargetGO);
	}

	void EnableImageTargetGO ()
	{
		imageTargetGO.SetActive (true);
	
	}
	void DisableImageTargetGO ()
	{
		imageTargetGO.SetActive (false);
		StopAllCoroutines ();
	}
}

