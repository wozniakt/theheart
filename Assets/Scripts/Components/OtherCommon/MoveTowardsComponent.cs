﻿using UnityEngine;
using System.Collections;

public class MoveTowardsComponent : MonoBehaviour,IMovable
{
	public Transform Target;
	public float speed;
	public bool isMoving;
	#region IMovable implementation

	public void StartMove ()
	{
		
		isMoving = true;
	}


	public void StartMove (Transform target)
	{
		Target = target;
		isMoving = true;
	}

	public void StopMove ()
	{
		Target = null;
		isMoving = true;
	}

	#endregion

	void Update() {
		if (isMoving) {
			float step = speed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, Target.position, step);
		}

	}
}
