﻿using UnityEngine;
using System.Collections;

public class DisableAfterTime : MonoBehaviour
{
	[SerializeField]
	float delay=2f;

	// Use this for initialization
	void OnEnable ()
	{
		StartCoroutine (Disabling ());
	}
	void OnDisable()
	{
		StopAllCoroutines ();
	}
	
	// Update is called once per frame
	IEnumerator Disabling ()
	{
		yield return new WaitForSeconds (delay);
		gameObject.SetActive (false);
	}
}

