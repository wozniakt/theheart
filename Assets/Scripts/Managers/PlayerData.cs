﻿using UnityEngine;
using System.Collections;

public class PlayerData : MonoBehaviour
{
	public PlayerModel CurrentPlayer;
	public GameObject HeartGO;
	void Awake()
	{
		CurrentPlayer.CurrentPoints = 0;
		CurrentPlayer.HighscorePoints = PlayerPrefs.GetInt ("Highscore", 0);
		HeartGO.GetComponent<IHealth> ().Health = CurrentPlayer.MaxHealth;
		HeartGO.GetComponent<IHealth> ().MaxHealth = CurrentPlayer.MaxHealth;
		EventsManager.StartListening ("PointsChange",PointsChange);
	}
	void OnEnable()
	{
		CurrentPlayer.CurrentPoints = 0;
	}

	void PointsChange ()
	{
		CurrentPlayer.CurrentPoints++;
		CurrentPlayer.HighscorePoints = Mathf.Max (CurrentPlayer.HighscorePoints, CurrentPlayer.CurrentPoints);
		PlayerPrefs.SetInt ("Highscore", CurrentPlayer.HighscorePoints);
	}
}

