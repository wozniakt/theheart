﻿using UnityEngine;
using System.Collections;

public class FactorySingletons : MonoBehaviour
{
	public static FactorySingletons Instance;
	public EventsManager EventsManager;
	public PlayerData PlayerData;
	public PoolManager PoolManager;

	void Awake ()
	{
		if (Instance == null) {
			Instance = this;
			DontDestroyOnLoad (this);
		} else {
			if (this != Instance)
				Destroy (this.gameObject);
		}
	}

}

